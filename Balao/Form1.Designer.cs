﻿namespace Balao
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCriaBalao = new System.Windows.Forms.Button();
            this.cboCor = new System.Windows.Forms.ComboBox();
            this.cboDirecao = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblAltura = new System.Windows.Forms.Label();
            this.rBSubir = new System.Windows.Forms.RadioButton();
            this.rBDescer = new System.Windows.Forms.RadioButton();
            this.btnAtvaPrograma = new System.Windows.Forms.Button();
            this.lblResultado = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCriaBalao
            // 
            this.btnCriaBalao.Enabled = false;
            this.btnCriaBalao.Location = new System.Drawing.Point(7, 7);
            this.btnCriaBalao.Name = "btnCriaBalao";
            this.btnCriaBalao.Size = new System.Drawing.Size(141, 72);
            this.btnCriaBalao.TabIndex = 0;
            this.btnCriaBalao.Text = "Criar Balao";
            this.btnCriaBalao.UseVisualStyleBackColor = true;
            this.btnCriaBalao.Click += new System.EventHandler(this.btnCriaBalao_Click);
            // 
            // cboCor
            // 
            this.cboCor.FormattingEnabled = true;
            this.cboCor.Location = new System.Drawing.Point(7, 86);
            this.cboCor.Name = "cboCor";
            this.cboCor.Size = new System.Drawing.Size(141, 21);
            this.cboCor.TabIndex = 1;
            // 
            // cboDirecao
            // 
            this.cboDirecao.FormattingEnabled = true;
            this.cboDirecao.Location = new System.Drawing.Point(7, 114);
            this.cboDirecao.Name = "cboDirecao";
            this.cboDirecao.Size = new System.Drawing.Size(141, 21);
            this.cboDirecao.TabIndex = 2;
            this.cboDirecao.SelectedIndexChanged += new System.EventHandler(this.cboDirecao_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(164, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(271, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // lblAltura
            // 
            this.lblAltura.AutoSize = true;
            this.lblAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltura.Location = new System.Drawing.Point(184, 146);
            this.lblAltura.Name = "lblAltura";
            this.lblAltura.Size = new System.Drawing.Size(15, 20);
            this.lblAltura.TabIndex = 4;
            this.lblAltura.Text = "-";
            // 
            // rBSubir
            // 
            this.rBSubir.AutoSize = true;
            this.rBSubir.Location = new System.Drawing.Point(194, 180);
            this.rBSubir.Name = "rBSubir";
            this.rBSubir.Size = new System.Drawing.Size(49, 17);
            this.rBSubir.TabIndex = 5;
            this.rBSubir.TabStop = true;
            this.rBSubir.Text = "Subir";
            this.rBSubir.UseVisualStyleBackColor = true;
            this.rBSubir.CheckedChanged += new System.EventHandler(this.rBSubir_CheckedChanged);
            // 
            // rBDescer
            // 
            this.rBDescer.AutoSize = true;
            this.rBDescer.Location = new System.Drawing.Point(194, 204);
            this.rBDescer.Name = "rBDescer";
            this.rBDescer.Size = new System.Drawing.Size(59, 17);
            this.rBDescer.TabIndex = 6;
            this.rBDescer.TabStop = true;
            this.rBDescer.Text = "Descer";
            this.rBDescer.UseVisualStyleBackColor = true;
            // 
            // btnAtvaPrograma
            // 
            this.btnAtvaPrograma.Enabled = false;
            this.btnAtvaPrograma.Location = new System.Drawing.Point(7, 175);
            this.btnAtvaPrograma.Name = "btnAtvaPrograma";
            this.btnAtvaPrograma.Size = new System.Drawing.Size(141, 52);
            this.btnAtvaPrograma.TabIndex = 7;
            this.btnAtvaPrograma.Text = "Ativar Programa";
            this.btnAtvaPrograma.UseVisualStyleBackColor = true;
            this.btnAtvaPrograma.Click += new System.EventHandler(this.btnAtvaPrograma_Click);
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultado.Location = new System.Drawing.Point(6, 238);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(9, 12);
            this.lblResultado.TabIndex = 8;
            this.lblResultado.Text = "-";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(294, 175);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(141, 52);
            this.btnStop.TabIndex = 9;
            this.btnStop.Text = "Parar Programa";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 261);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.btnAtvaPrograma);
            this.Controls.Add(this.rBDescer);
            this.Controls.Add(this.rBSubir);
            this.Controls.Add(this.lblAltura);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cboDirecao);
            this.Controls.Add(this.cboCor);
            this.Controls.Add(this.btnCriaBalao);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCriaBalao;
        private System.Windows.Forms.ComboBox cboCor;
        private System.Windows.Forms.ComboBox cboDirecao;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblAltura;
        private System.Windows.Forms.RadioButton rBSubir;
        private System.Windows.Forms.RadioButton rBDescer;
        private System.Windows.Forms.Button btnAtvaPrograma;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnStop;
    }
}

