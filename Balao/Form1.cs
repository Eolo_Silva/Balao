﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Timers;

namespace Balao
{

    public partial class Form1 : Form
    {
        int kilometros, metros, centimetros;
        Globo globo = new Globo();
        

        public Form1()
        {
            InitializeComponent();
            cboCor.Items.AddRange(new string[] { "Vermelho", "Azul", "Amarelo" });
            cboDirecao.Items.AddRange(new string[] { "Norte", "Sul", "Este", "Oeste" });
            timer1.Enabled = true;
           

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnCriaBalao_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = true;

            if (cboCor.SelectedIndex == 0)
            {
                pictureBox1.Image = Image.FromFile(@"...\image\Vermelho.jpg");
            }
            if (cboCor.SelectedIndex == 1)
            {
                pictureBox1.Image = Image.FromFile(@"...\image\Azul.jpg");
            }
            if (cboCor.SelectedIndex == 2)
            {
                pictureBox1.Image = Image.FromFile(@"...\image\Amarelo.jpg");
            }
        }

        private void cboDirecao_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboCor.Enabled = false;
            btnCriaBalao.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            globo.Altura = lblAltura.Text;
            globo.Cor = cboCor.Text;
            globo.Direcao = cboDirecao.Text;

            if (cboCor.Text == "Vermelho" || cboCor.Text == "Azul" || cboCor.Text == "Amarelo")
            {
                if (rBSubir.Checked)
                {

                    centimetros++;

                    if (centimetros == 1000)
                    {
                        metros++;
                        centimetros = 0;
                    }
                    else if (metros == 1000)
                    {
                        kilometros++;
                        metros = 0;
                    }
                   
                }        
                if (rBDescer.Checked)
                {
                    centimetros--;

                    if (centimetros == 1000)
                    {
                        metros--;
                        centimetros = 0;
                    }
                    if (metros == 1000)
                    {
                        kilometros--;
                        metros = 0;
                    }
                    else if (kilometros == 000 && metros == 000 && centimetros == 000)
                    {
                        Application.Exit();
                    }                  
                }
                lblAltura.Text = kilometros.ToString().PadLeft(3, '0') + " Km " + metros.ToString().PadLeft(3, '0') + " mts " + centimetros.ToString().PadLeft(3, '0') + " ctms";
                lblResultado.Text = globo.ToString();
                cboCor.Enabled = true;
                cboDirecao.Enabled = true;               
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void rBSubir_CheckedChanged(object sender, EventArgs e)
        {
            cboDirecao.Enabled = false;
            btnAtvaPrograma.Enabled = true;
        }

        private void btnAtvaPrograma_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            cboCor.Enabled = true;
            cboDirecao.Enabled = true;
        }
    }
}

