﻿using System;
using System.Timers;


namespace Balao
{
    class Globo
    {
        #region Atributos
        private string _cor;
        private string _altura;
        private string _direcao;
        #endregion

        #region propriedades
        public string Cor { get; set; }
        public string Altura { get; set; }
        public string Direcao { get; set; }
        #endregion

        #region Construtores
        //public Globo()
        //{
        //    Cor = "Vermelho";
        //    Altura = 1000;
        //    Direcao = "Norte";
        //}

        public Globo() : this("","","") { }

        public Globo(string cor, string altura, string direcao)
        {
            Cor = cor;
            Altura = altura;
            Direcao = direcao;
        }
        public Globo(Globo BG) : this(BG.Cor, BG.Altura, BG.Direcao)
        {

        }
        #endregion

        #region Metodos Gerais
        public override string ToString()
        {
            return String.Format("O Globo {0}, Atingiu {1} de altura, indo na direção {2}", Cor, Altura, Direcao);
        }
        #endregion

        #region Outros Metodos

 
        #endregion
    }
}
